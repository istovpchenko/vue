import { AUTH_REQUEST, AUTH_LOGOUT, AUTH_SUCCESS, AUTH_ERROR } from '../actions/auth';
import { USER_REQUEST } from "../actions/user";

const axios = require('axios');

/*import createAuth0Client from "@auth0/auth0-spa-js";

var authClient;
createAuth0Client({
    domain: 'lifetec.eu.auth0.com',
    client_id: "0qYYEWbB5Q0k8dNGTJk3M4bM67cO22Ne",
    redirect_url: "http://localhost:8080/"
}).then(client => {
        authClient = client;
    }
);*/

const authClient = new class {
    getTokenWithPopup() {return new Promise((resolve) => {
            resolve('that-is-the-fake-token');
        })}
};

const state =  {
    token: localStorage.getItem('user-token')||'',
    status: '',
};

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status
};

const actions = {
    [AUTH_REQUEST]:({commit,dispatch}) =>{
        return new Promise((resolve, reject)=>{
            commit(AUTH_REQUEST);
            authClient.getTokenWithPopup()
                .then(resp => {
                    const token = resp; ////.data.token
                    localStorage.setItem('user-token', token);
                    axios.defaults.headers.common['Authorization'] = token;
                    commit(AUTH_SUCCESS,resp);
                    dispatch(USER_REQUEST); //????????????????????????
                    resolve(resp);
            //        return(resp)
                })
                .catch(err => {
                    commit(AUTH_ERROR, err);
                    localStorage.removeItem('user-token');
                    reject(err)
          //          return(err)
                })
        })
    },
    [AUTH_LOGOUT]:({commit}) =>{ /// dispatch
        return new Promise((resolve) => {
            commit(AUTH_LOGOUT);
            //authClient.logout()
            //    .catch(err => { reject(err) });
            localStorage.removeItem('user-token');
            delete axios.defaults.headers.common['Authorization'];
            resolve()
        })
    }
};

const mutations = {
    [AUTH_REQUEST]: (state) => {
        state.status = 'loading';
    },
    [AUTH_SUCCESS]: (state, token) => {
        state.status = 'success';
        state.token = token;
        alert(state.token)
    },
    [AUTH_ERROR]: (state) => {
        state.status = 'error';
    },
    [AUTH_LOGOUT]: (state) => {
        state.token = '';
        state.status = '';
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}

