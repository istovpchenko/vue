import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css'

import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(Buefy);

const axios = require('axios');
const token = localStorage.getItem('user-token');

if (token) {
  axios.defaults.headers.common['Authorization'] = token;
}

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
